import os
import numpy as np

# this library is to provide common methods
# with a convenient way (use one function to wrap many steps)

# Author:   perry92205
# Contact:  voldemort92205@gmail.com
# news:
#    2019/06/10: create function "create_folder"

def create_folder (path, verbose: bool = True):
# Versions:
#    ver1: 2019/06/10
#        create function "create_folder"
#    ver2: 2019/10/24
#        set verbose

	if not os.path.exists (path):
		os.makedirs (path)
	else:
		if verbose:
			print ("The folder (%s) already exist" %path)

def check_file (path):
# given a path (absolute path), check whether the file exists!!
# 1: exist, 0: no (return integer)
	return int (os.popen ("test -e " + path + " && echo 1 || echo 0").read ().split ("\n")[0])

def get_lonlat_range (x, x1, x2):
# Versions:
#    ver1: 2019/06/10
#        create function "get_lonlat_range"
	tmp = np.asarray (x)
	x_region = np.argwhere ((tmp >= x1) & (tmp <= x2))
	y1 = x_region [0][0]
	y2 = x_region [-1][0]
	return [y1, y2]

def get_area (lon, lat):
# Versions:
#    ver1: 2020/04/15
#        create function "get_area"
#
# reference: https://www.ncl.ucar.edu/Document/Functions/Built-in/wgt_areaave.shtml

	nlon, nlat = lon.shape [0], lat.shape [0]
	rad = np.pi / 180
	re = 6371220.0
	rr = re * rad

	dlon = abs (lon[2] - lon[1]) * rr
	dx = dlon * np.cos (lat * rad)

	dy = np.zeros (nlat, dtype = dx.dtype)
	dy [0] = abs (lat[2]-lat[1]) * rr
	dy [1:-1] = abs (lat [2:] - lat [:-2]) * rr * 0.5
	dy [-1] = abs (lat[-1] - lat[-2]) * rr

	area = dx * dy
	return area

def get_weight_matrix (lat, nlon, other_dim):
# Versions:
#    ver1: 2019/06/17
#        create function "get_weight_matrix"
#        lat should be 1-d array
	wt2d = set_2d_weight_matrix (lat, nlon)
	return np.squeeze (np.tile (wt2d, np.append (other_dim, (1, 1))))

def set_2d_weight_matrix (lat, nlon):
	weights = np.transpose (np.tile (np.cos (np.radians (lat)), (nlon, 1)), (1, 0))
	return weights / np.sum (weights) * (nlon * lat.shape[0])
