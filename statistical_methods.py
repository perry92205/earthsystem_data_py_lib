import numpy as np
import scipy.stats as stats

'''
Author: perry92205
Contact: voldemort92205@gmail.com

Comment:
    This provides the statistical methods that can calculate higher dimension variables
    or calculate some rarely used statistical methods
'''

def rmse_ndim (var1, var2, axis: int = 0):
	output = {}
	output ["rmse"] = np.ma.sqrt ( ((var1-var2)**2).mean (axis = axis) )

	return output

def corrcoef_spearman_ndim (var1, var2, axis: int = 0):
	'''
	Versions:
		ver1: 2019/12/04
			created
	'''

	var1_order = var1.argsort (axis = axis).argsort (axis = axis) + 1
	var2_order = var2.argsort (axis = axis).argsort (axis = axis) + 1
	d2 = (var1_order - var2_order) ** 2

	# get sample size n
	ndata = (1-np.ma.getmaskarray(var1).astype(int)).sum (axis = axis)

	# calculate corr_spearman
	corr = 1 - (6 * d2.sum (axis = axis)) / (ndata * (ndata**2-1))

	#
	pval = None

	output = {}
	output ["corrcoef"] = corr
	output ["pval"] = pval
	output ["ndata"] = ndata
	return output



def corrcoef_ndim (var1, var2, axis: int = 0):
	'''
	Versions:
		ver1: 2019/12/04
			created
	'''
	# --- corrcoef = cov(x, y) / sqrt (std(x) * std(y)) ---
	numerator = np.ma.mean ((var1 - var1.mean(axis = axis, keepdims = True)) * (var2 - var2.mean(axis = axis, keepdims = True)), axis = axis)
	denominator = var1.std(axis = axis) * var2.std(axis = axis)
	corr = numerator / denominator

	# set max of corr as 1 or -1
	corr = np.ma.array (np.ma.where (np.ma.abs(corr) <= 1, corr, np.sign(corr) * 1.0), mask = np.ma.getmaskarray (corr))

	# --- calculate P-value ---
	# --- Not yet implemented
	pval = None

	# --- set return variable ---
	'''
	# output structure
	corrcoef
	pval
	ndata for the given axis, each grid can have different ndata
	'''

	output = {}
	output ["corrcoef"] = corr
	output ["pval"] = pval
	output ["ndata"] = np.ma.sum (1-np.ma.getmaskarray(var1).astype(int), axis = axis)
	return output

def fisher_transform (corr):
	# Z = 0.5 * ln((1+r) / (1-r))
	return 0.5 * np.ma.log ((1+corr)/(1-corr))

def corrcoef_significant_diff_ndim (corr1, corr2, alpha = 0.1):
	'''
	Versions:
		ver1: 2019/12/04
			created
	'''
	'''
	Comment:
	    test whether "corr1" is significantly different from "corr2" under the alpha confidence level
	    default confidence is 90%
	    the structure of variables "corr1", and "corr2" follows the function "corrcoef_ndim"
	'''
	# use fisher transformation to calculate z-value
	zval1 = fisher_transform (corr1["corrcoef"])
	zval2 = fisher_transform (corr2["corrcoef"])

	# sigma = np.sqrt (1/(n1-3) + 1/(n2-3))
	sigma_all = np.ma.sqrt (1/(corr1["ndata"]-3) + 1/(corr2["ndata"]-3))

	# calculate pval
	zval = (zval1 - zval2) / sigma_all

	# calculate p-values for a given alpha and determine whether it is significant
	pval = np.ma.array (stats.norm.cdf (-np.ma.abs(zval)), mask = np.ma.getmaskarray (zval))
	is_significant = np.ma.array (np.where (pval < alpha/2, True, False), mask = np.ma.getmaskarray (zval))

	output = {}
	output ["zval"] = zval
	output ["pval"] = pval
	output ["significant_test"] = is_significant
	return output
