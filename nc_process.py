import numpy as np
from netCDF4 import Dataset

# this library is to convert data from netcdf to numpy array
# with a convenient way (use one function to wrap many steps)

# Author:   perry92205
# Contact:  voldemort92205@gmail.com
# news:
#    2018/10/31: create function "nc_read_process"

def nc_read_process (path, var, missing_value = np.nan):
# Versions:
#    ver1: 2018/10/31
#        create function "nc_read_process"
#            This function convert masked array to ndarray
#            When converted, the _FillValue will be replaced as np.nan
#    ver2: 2018/11/23
#        user-defined missing_value is used
# var (string) should be a variable name in your ncfile (path to your file)

    f = Dataset (path, "r")
    tmp = np.squeeze (f.variables [var][:])
    if (np.ma.is_masked (tmp)):
        # convert _FillValue as np.nan
        output = tmp.data
        output [output == tmp._fill_value] = missing_value
    else:
        output = tmp
    return output
