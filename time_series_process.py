import numpy as np

def calculate_pentad_mean_3d (var):
	(ndays, nlat, nlon) = var.shape
	var_pentad = np.ma.zeros ((73, nlat, nlon), dtype = var.dtype)

	# calculate pentad
	day_pentad = np.arange (0, ndays - 1, 5)
	if ndays == 366:
		# leap year
		# 2/25 - 3/1 contains 6 days, which is on the 12-th pentad
		# so we should shift the begin index after 13-th pentad (including this pentad)
		day_pentad [12:] = day_pentad [12:] + 1
	day_pentad = np.append (day_pentad, ndays)

	for ipentad in range (73):
		t1, t2 = day_pentad [ipentad], day_pentad [ipentad + 1]
		var_pentad [ipentad, :, :] = np.ma.average (var [t1:t2, :, :], axis = 0)
	return var_pentad

def get_pentad_time (var_time):
	ndays = var_time.shape [0]
	day_pentad = np.arange (0, ndays - 1, 5)
	if ndays == 366:
		# leap year
		day_pentad [12:] = day_pentad [12:] + 1
	return var_time [day_pentad]
def get_ith_pentad_day_range (ith, leap_year = False):
	if ith > 73 or ith <= 0:
		print ("Out of range (1 ~ 73)")
		exit ()
	
	day_begin, day_end = ith * 5, (ith+1) * 5
	if leap_year and ith == 12:
		# include 6 days
		day_end += 1
	elif leap_year and ith > 12:
		# shift one day
		day_begin, day_end = day_begin+1, day_end + 1
	return [day_begin, day_end]

def convert_daily_to_monthly_1yr (var):
	ntime = var.shape [0]
	if ntime != 365 and ntime != 366:
		print ("Error: time should be given a year!!")
		exit ()
	day_of_mon = np.array ([31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31])
	if ntime == 366:
		day_of_mon [1] = 29
	current = 0
	output = []
	for imon in range (12):
		t1, t2 = current, current + day_of_mon [imon]
		output.append (np.ma.average (var [t1:t2, :, :], axis = 0))
		current += day_of_mon [imon]
	return np.ma.array (output)

def convert_daily_to_monthly_1yr_mon_range (var, begin_mon, end_mon, this_year, isleap = None):
	if begin_mon <= 0 or end_mon > 12 or begin_mon > end_mon:
		print ("\x1b[0;31;40m" + "Error: begin_mon, end_mon should be in range of [1, 12]" + "\x1b[0m")
		exit ()

	day_of_mon = np.array ([31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31])
	if isleap is None:
		if this_year % 400 == 0 or (this_year % 4 == 0 and this_year % 100 != 0):
			# leap year
			day_of_mon [1] = 29
	elif isleap == True:
		# user believes that this is leap year
		day_of_mon [1] = 29
	# TODO: vectorization
	current = 0
	output = []
	for tmon in range (begin_mon-1, end_mon):
		t1, t2 = current, current + day_of_mon [tmon]
		output.append (np.ma.average (var [t1:t2, :, :], axis = 0))
		current += day_of_mon [tmon]
	return np.ma.array (output, dtype = var.dtype)
