from datetime import timedelta, datetime

# this library is to calculate day with different calendar

# Author:   perry92205
# Contact:  voldemort92205@gmail.com
# News:
#    2018/11/07: create function "get_gregorian"

def get_gregorian (year, month, day):
# Versions:
#    ver1: 2018/11/7
#        create function "get_gregorian"

    base_year = 1601
    diff_year = year - base_year

    nleap = diff_year // 4 - diff_year // 100 + diff_year // 400
    doy = (datetime (year, month, day, 0, 0, 0) - datetime (year, 1, 1, 0, 0, 0)).days + 1
    totaldays = doy + 365 * (diff_year) + nleap
    return totaldays
