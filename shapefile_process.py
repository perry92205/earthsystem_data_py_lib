import numpy as np
import multiprocessing as mp

def select_mask (lon, lat, spflon, spflat, geometry, segments):
	# make sure that lon (lat) is large than the spflon (lat)
	bound_mask = np.ma.zeros ((lat.shape [0], lon.shape [0]), dtype = int)

	pool = mp.Pool (mp.cpu_count ())
	bdpt = np.array ([lon[0]-5, lat[0]-5]) # this point is out of boundary
	for ix in range (lon.shape [0]):
		bound_mask [:, ix] = np.array ([pool.starmap (select_county, [(bdpt,\
										np.array ([lon[ix], lat[iy]]), spflon, spflat, geometry, segments) \
													for iy in range (lat.shape [0])])], dtype = int)
	return bound_mask

def select_county (bdpt, A, spflon, spflat, geometry, segments) -> int:
	if A [1] < np.min (spflat) or A [1] > np.max (spflat)\
	or A [0] < np.min (spflon) or A [0] > np.max (spflon):
		# out of boundary
		return 0
	for ifeature in range (geometry.shape [0]):
		segment1 = geometry [ifeature, 0]
		nsegment = geometry [ifeature, 1]
		for isegment in range (segment1, segment1 + nsegment):
			loc1 = segments [isegment, 0]
			nloc = segments [isegment, 1]
			locs_x = spflon [loc1:loc1+nloc]
			locs_y = spflat [loc1:loc1+nloc]
			if is_in_boundary (bdpt, A, locs_x, locs_y):
				return ifeature + 1
	# no found --> ocean
	return 0

def is_in_boundary (bdpt, A, spflon, spflat) -> bool:
	locs = np.transpose (np.array ([spflon, spflat]))
	nloc = spflon.shape [0]
	count = 0
	if A [1] < np.min (spflat) or A [1] > np.max (spflat)\
	or A [0] < np.min (spflon) or A [0] > np.max (spflon):
		# out of boundary of this Island
		return False
	for iloc in range (nloc-1):
		c1 = locs [iloc, :]
		c2 = locs [iloc+1, :]
		if is_intersection_2segments (bdpt, A, c1, c2):
			count += 1
	return (count % 2 == 1)

def vcross (a, b):
	return (a[0]*b[1] - a[1]*b[0])

def vdot (a, b):
	return (a[0]*b[0]+a[1]*b[1])

def is_intersection_2segments (a1, a2, b1, b2) -> bool:
	clockwise1 = np.sign (vcross (b1-a1, a2-b1))
	clockwise2 = np.sign (vcross (a2-b1, b2-a2))
	clockwise3 = np.sign (vcross (b2-a2, a1-b2))
	clockwise4 = np.sign (vcross (a1-b2, b1-a1))

	if np.abs (clockwise1 + clockwise2 + clockwise3 + clockwise4) == 4:
		# same direction
		return True
	if np.abs (clockwise1 + clockwise2 + clockwise3 + clockwise4) == 3:
		# one point in one line is located on the ohter line
		# one clockwise is 0
		return True
	if clockwise1 == 0 and clockwise2 == 0 and clockwise3 == 0 and clockwise4 == 0:
		signal1 = np.sign (vdot (b1-a1, a2-b1))
		signal2 = np.sign (vdot (a2-b1, b2-a2))
		signal3 = np.sign (vdot (b2-a2, a1-b2))
		signal4 = np.sign (vdot (a1-b2, b1-a1))
		if signal1 + signal2 + signal3 + signal4 == -4:
			return False
		else:
			return True
	# other cases
	return False
